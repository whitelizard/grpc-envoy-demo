"""Service1 gRPC servicer."""
import ServiceRpc_pb2_grpc as rpc
import ServiceRpc_pb2 as msg


class Servicer(rpc.Service1Servicer):
    """Servicer implementing the gRPC API."""

    def __init__(self):
        super().__init__()

    def Function(self, request, context):
        """Return a response."""
        print('Received request')
        return msg.Response(result=1)

# grpc-envoy-demo

## Getting started

- Install Envoy
- Run `make`
- Start Envoy with config: `envoy -c envoy.yaml -l trace`
- Run the server: `python3 service1.py`

Now run the client request script: `python3 grpc-client-test.py`

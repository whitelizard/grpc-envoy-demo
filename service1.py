"""gRPC service."""
import grpc
import concurrent.futures
import grpc_servicer

import ServiceRpc_pb2_grpc as rpc

servicer = grpc_servicer.Servicer()
executor = concurrent.futures.ThreadPoolExecutor(
    thread_name_prefix="gRPC-worker"
)
server = grpc.server(executor)
rpc.add_Service1Servicer_to_server(servicer, server)
server.add_insecure_port("127.0.0.1:8000")
server.start()
print('Service has started')
server.wait_for_termination()

"""Test run grpc client."""
import grpc

import ServiceRpc_pb2_grpc as rpc
import ServiceRpc_pb2 as msg


channel = grpc.insecure_channel("127.0.0.1:10000")
stub = rpc.Service1Stub(channel)
response = stub.Function(msg.Command(cmd="test"))
print(response)
